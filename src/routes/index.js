import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import dashboards from "./dashboards";

class AppMainProtectedRoutes extends Component {
  render() {
    const { match } = this.props;
    return (
      <div>
        <Switch>
          <Route path={`${match.url}/dashboards`} component={dashboards} />
          <Redirect to="/error" />
        </Switch>
      </div>
    );
  }
}

export default AppMainProtectedRoutes;
