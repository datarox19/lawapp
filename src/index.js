import React from "react";
import ReactDOM from "react-dom";
//Must come first
import "./assets/css/vendor/bootstrap.min.css";
import "./assets/css/sass/themes/lowapp.light.purple.scss";
import MainApp from "./App";
const rootEl = document.getElementById("root");
ReactDOM.render(<MainApp />, rootEl);
