export const daysToHours = (day = 0) => parseFloat(day) * 8 || 0;
