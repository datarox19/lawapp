import React, { Component } from "react";
import { Nav, NavItem, NavLink } from "reactstrap";
class TopNav extends Component {
	render() {
		return (
			<nav className="navbar fixed-top">
				<a href="/" className="navbar-logo">
					<span className="logo"> LawApp</span>
				</a>
				<div className="ml-auto">
					<Nav>
						<NavItem>
							<NavLink to={"/"}>Inscription client</NavLink>
						</NavItem>
						<NavItem>
							<NavLink to={"/"}> Inscription Avocat </NavLink>
						</NavItem>
						<NavItem>
							<NavLink to={"/"}>Connexion Client !</NavLink>
						</NavItem>
						<NavItem>
							<NavLink to={"/"}>Connexion Avocat</NavLink>
						</NavItem>
					</Nav>
				</div>
			</nav>
		);
	}
}

export default TopNav;
