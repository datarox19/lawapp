import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import TopNav from "containers/TopNav";
import Sidebar from "containers/Sidebar";

import AppMainProtectedRoutes from "../routes";
import login from "routes/layouts/login";
import Home from "routes/layouts/home";
import error from "routes/layouts/error";

const ProtectedRoute = ({ component: Component, authUser, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      authUser ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

class App extends Component {
  render() {
    const { match } = this.props;
    return (
      <div
        id="app-container"
        className={"menu-default menu-sub-hidden main-hidden sub-hidden"}
      >
        <TopNav />
        {/*<Sidebar />*/}
        <main>
          <div className="container-fluid">
            <Switch>
              <Route path={`/`} exact component={Home} />
              <Route path={`/login`} component={login} />
              <ProtectedRoute
                path={`${match.url}app`}
                authUser={true}
                component={AppMainProtectedRoutes}
              />
              <Route path={`/error`} component={error} />
              <Redirect to="/error" />
            </Switch>
          </div>
        </main>
      </div>
    );
  }
}

export default App;
