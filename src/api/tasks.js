import { db } from "../firebase";

const taskRef = (taskID = null) => {
	let path = taskID ? `tasks/${taskID}` : "tasks";
	return db.ref(path);
};

const userTaskRef = (taskID = null) => {
	let path = taskID ? `userTasks/${taskID}` : "userTasks";
	return db.ref(path);
};

const projectTaskRef = (taskID = null) => {
	let path = taskID ? `projectTasks/${taskID}` : "projectTasks";
	return db.ref(path);
};

const addTaskRequest = async task => {
	const { taskID } = task;
	//Si taskID exist dans l'object {task} je suppose qu'il s'agit d'une edit action
	if (taskID) {
		return editTaskRequestApi(task);
	}
	task.isDeleted = false;
	//je ne veux pas que le {taskID:key} sera enregistrer dans la table
	//le key du document tasks est l'ID tache
	delete task["taskID"];
	return await taskRef()
		.push(task)
		.then(ref => ref.key)
		.catch(error => error);
};

const editTaskRequestApi = async task => {
	const taskID = task.taskID;
	delete task[taskID];
	return await taskRef(taskID)
		.update(task)
		.then(() => task)
		.catch(error => error);
};


const editMultiTaskRequest = async task => {
	const taskID = task.taskID;
	delete task[taskID];
	return await taskRef(taskID)
		.update(task)
		.then(() => task)
		.catch(error => error);
};

/**
 * Supprimer une tache
 * @param String taskID: tache ID
 */
const deleteTaskRequestApi = async (taskID, isDeleted = true) => {
	return await taskRef(taskID)
		.update({ isDeleted })
		.then(() => taskID)
		.catch(error => error);
};

const undoDeleteTaskRequestApi = async task => {
	return await taskRef(task)
		.update({ isDeleted: false })
		.then(() => task)
		.catch(error => error);
};

const getProjectTasks = projectID =>
	projectTaskRef(projectID)
		.orderByChild("projectID")
		.equalTo(projectID);

/**
 * Document projet
 * @param {*} id
 */
const getSingleTask = async id => await taskRef(id).once("value");

export {
	taskRef,
	userTaskRef,
	deleteTaskRequestApi,
	undoDeleteTaskRequestApi,
	editTaskRequestApi,
	addTaskRequest,
	getProjectTasks,
	getSingleTask,
	editMultiTaskRequest
};
