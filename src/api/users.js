import { db } from "../firebase";
//UID Taha: WyMjDTijBodnbNE5e
/**
 *
 */
const usersRef = (ID = null) => {
	let path = ID ? `users/${ID}` : "users";
	return db.ref(path);
};

const editUserRequestApi = async (user, role) => {
	role = role ? role : "customer";
	const ID = user.collaborateurID || user.ID;
	delete user["collaborateurID"];
	delete user["ID"];
	user.roles = {
		[role]: true
	};
	return await usersRef(ID)
		.update(user)
		.then(() => user)
		.catch(error => error);
};

/**
 * Création nouveau client
 * @param {*} customer
 */
const addCustomerRequestApi = async customer => {
	const { ID } = customer;

	if (ID) {
		customer.updatedAt = new Date().getTime();
		return editUserRequestApi(customer);
	}
	delete customer["ID"];
	customer.createdAt = new Date().getTime();
	return await usersRef()
		.push(customer)
		.then(ref => ref.key)
		.catch(error => error);
};

const addUserByRoleRequestApi = async (user, role) => {
	return await usersRef()
		.push(user)
		.then(ref => ref.key)
		.catch(error => error);
};

const getUsersByRoleOnce = role => {
	return usersRef()
		.orderByChild(`permission/key`)
		.equalTo(role)
		.once("value");
};

const getUsersByRole = role =>
	usersRef()
		.orderByChild(`permission/key`)
		.equalTo(role);

/**
 * Supprimer un utilisateur
 * @param String ID
 */
const deleteRequestApi = async (ID, isDeleted = true) => {
	console.log(ID, isDeleted);
	return await usersRef(ID)
		.update({ isDeleted })
		.then(() => ID)
		.catch(error => error);
};

/**
 * Annuler la suppression d'un utilisateur
 * @param {A} user
 */
const undoDeleteRequestApi = async user => {
	return await usersRef(user)
		.update({ isDeleted: false })
		.then(() => user)
		.catch(error => error);
};

const checkIfUserAlreadyExist = async email => {
	return await usersRef()
		.orderByChild("email")
		.equalTo(email)
		.once("value");
};

const getUserByUid = uid => usersRef(uid).once("value");

/**
 *
 * @param {*} userID
 * @param {*} count
 */
const getLastInsertedTasks = async (userID, count = 5) =>
	await db
		.ref(`users/${userID}/tasks`)
		.limitToLast(count)
		.on("value")
		.then(doc => doc.key);

export {
	addCustomerRequestApi,
	getUsersByRole,
	getUsersByRoleOnce,
	addUserByRoleRequestApi,
	deleteRequestApi,
	undoDeleteRequestApi,
	checkIfUserAlreadyExist,
	getLastInsertedTasks,
	editUserRequestApi,
	getUserByUid
};
