import { db } from "../firebase";
import { toast } from "react-toastify";

const categoriesRef = (ID = null) => {
	let path = ID ? `categories/${ID}` : "categories";
	return db.ref(path);
};
/**
 * Création nouvelle catégorie
 * @param Object category
 */
const addCategoryRequestApi = async category => {
	category.createdAt = new Date().getTime();
	return await categoriesRef()
		.push(category)
		.then(ref => ref.key)
		.catch(error => error);
};
/**
 * Modification d'une categorie
 * @param {*} expertise
 */
 const editCategorieRequestApi = async (category)  => {
 	const ID = category.categorieID;
 	delete category["categorieID"];

	 	return await categoriesRef(ID)
	 		.update(category)
	 		.then(() => category)
	 		.catch(error => error);

	 };
/**
 *
 */

 /**
  * Supprimer une categorie
  * @param String ID
  */
 const deleteRequestApi = async (ID) => {
 	return await categoriesRef(ID)
 		.remove()
 		.then(() => toast(`🦄 Bien! Categorie bien supprimé`))
 		.catch(error => error);
 };

 /**
  * Annuler la suppression d'un utilisateur
  * @param {A} user
  */
 const undoDeleteRequestApi = async user => {
 	return await categoriesRef(user)
 		.update({ isDeleted: true })
 		.then(() => user)
 		.catch(error => error);
 };

const getCategories = () => categoriesRef();

export { addCategoryRequestApi, editCategorieRequestApi, getCategories, deleteRequestApi, undoDeleteRequestApi };
