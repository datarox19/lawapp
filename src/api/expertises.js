import { db } from "../firebase";
import { toast } from "react-toastify";

const expertisesRef = (ID = null) => {
	let path = ID ? `expertises/${ID}` : "expertises";
	return db.ref(path);
};
/**
 * Création nouvelle expertise
 * @param {*} expertise
 */
const addExpertiseRequestApi = async expertise => {
	expertise.createdAt = new Date().getTime();
	return await expertisesRef()
		.push(expertise)
		.then(ref => ref.key)
		.catch(error => error);
};

/**
 * Modification d'une expertise
 * @param {*} expertise
 */
 const editExpertiseRequestApi = async (expertise)  => {
 	const ID = expertise.expertiseID;
 	delete expertise["expertiseID"];
	expertise.category = expertise.category.key;


 	return await expertisesRef(ID)
 		.update(expertise)
 		.then(() => expertise)
 		.catch(error => error);

 };


  /**
   * Supprimer une expertise
   * @param String ID
   */
  const deleteRequestApi = async (ID) => {
  	return await expertisesRef(ID)
  		.remove()
  		.then(() => toast(`🦄 Bien! Expertise bien supprimé`))
  		.catch(error => error);
  };
/**
 *
 */
const getExpertises = () => expertisesRef();

export { addExpertiseRequestApi, editExpertiseRequestApi, getExpertises, deleteRequestApi };
