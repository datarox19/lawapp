import { db } from "../firebase";
import { toast } from "react-toastify";

const villeRef = (ID = null) => {
	let path = ID ? `ville/${ID}` : "ville";
	return db.ref(path);
};
/**
 * Création nouvelle catégorie
 * @param Object ville
 */
const addVilleRequestApi = async ville => {
	console.log(ville)
	ville.createdAt = new Date().getTime();
	return await villeRef()
		.push(ville)
		.then(ref => ref.key)
		.catch(error => error);
};
/**
 * Modification d'une categorie
 * @param {*} expertise
 */
 const editVilleRequestApi = async (ville)  => {
 	const ID = ville.villeID;
 	delete ville["villeID"];

	 	return await villeRef(ID)
	 		.update(ville)
	 		.then(() => ville)
	 		.catch(error => error);

	 };
/**
 *
 */

 /**
  * Supprimer une categorie
  * @param String ID
  */
 const deleteRequestApi = async (ID) => {
 	return await villeRef(ID)
 		.remove()
 		.then(() => toast(`🦄 Bien! Ville bien supprimé`))
 		.catch(error => error);
 };

 /**
  * Annuler la suppression d'un utilisateur
  * @param {A} user
  */
 const undoDeleteRequestApi = async user => {
 	return await villeRef(user)
 		.update({ isDeleted: true })
 		.then(() => user)
 		.catch(error => error);
 };

const getVilles = () => villeRef();

export { addVilleRequestApi, editVilleRequestApi, getVilles, deleteRequestApi, undoDeleteRequestApi };
