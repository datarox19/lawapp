import { db } from "../firebase";
import uuidv1 from "uuid/v1";
import { TotalFromKeyObject } from "Util/Utils";
import { daysToHours } from "Util/Utils";
/**
 * Collection projets ou document project
 * @param String projectID : ID project dans firebase
 */
const projectsRef = (projectID = null) => {
	let path = projectID ? `projects/${projectID}` : "projects";
	return db.ref(path);
};

/**
 * Création nouveau projet
 * @param {*} projet
 */
const addProjectRequestApi = projet => {
	return new Promise((resolve, reject) => {
		projet.createdAt = new Date().getTime();
		projectsRef()
			.push(projet)
			.then(resolve, reject);
	});
};

const getProjects = () => projectsRef().orderByChild("customerRaisonSocial");

/**
 * Document projet
 * @param {*} id
 */
const getSingleProject = id => projectsRef(id);

/**
 * Archiver un projet,
 * Si vous souhaiter UNDO archive project pass isArchived à false
 * @param String projectID
 * @param Boolean isArchived par default est true
 */
const ArchiverProject = async (projectID, isArchived = true) => {
	let isArchivedObject = {
		isArchived,
		isArchivedAt: new Date().getTime(),
		updatedAt: new Date().getTime()
	};
	return await projectsRef(projectID)
		.update(isArchivedObject)
		.then(() => projectID)
		.catch(error => error);
};

/**
 * Update Budget lines to add budget supplémentaire
 * Update Total Projet
 * @param {*} projectID
 * @param {*} budgetLines
 */
const addExtraBudgetLines = async (projectID, budgetLines) => {
	//Ajouter isExtra: true et générer un unique key pour chaque entré
	//L'objectif est d'enregistrer ces lignes de budgets séparément
	let bsArray = budgetLines.map(v => ({ [uuidv1()]: { ...v, isExtra: true } }));
	const getProject = await getSingleProject(projectID).once("value");
	const updateBudgetLigne = await projectsRef(projectID)
		.child("budgetLines")
		.update(Object.assign({}, ...bsArray));
	const somme = TotalFromKeyObject(budgetLines, "budget") || 0;
	const { TF, TNF, TP } = somme;
	return Promise.all([getProject, updateBudgetLigne, somme]).then(response => {
		return projectsRef(projectID).update({
			totalBudgetLineFacturable:
				response[0].child("totalBudgetLineFacturable").val() + daysToHours(TF),
			totalBudgetLineNonFacturable:
				response[0].child("totalBudgetLineNonFacturable").val() +
				daysToHours(TNF),
			totalBudgetPrestataireExterne:
				response[0].child("totalBudgetPrestataireExterne").val() + TP
		});
	});
};

export {
	addProjectRequestApi,
	getProjects,
	getSingleProject,
	ArchiverProject,
	addExtraBudgetLines
};
