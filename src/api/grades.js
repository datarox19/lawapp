import { db } from "../firebase";
import { toast } from "react-toastify";

const gradeRef = (ID = null) => {
	let path = ID ? `grade/${ID}` : "grade";
	return db.ref(path);
};
/**
 * Création nouvelle catégorie
 * @param Object grade
 */
const addGradeRequestApi = async grade => {
	grade.createdAt = new Date().getTime();
	return await gradeRef()
		.push(grade)
		.then(ref => ref.key)
		.catch(error => error);
};
/**
 * Modification d'une categorie
 * @param {*} expertise
 */
 const editGradeRequestApi = async (grade)  => {
 	const ID = grade.gradeID;
 	delete grade["gradeID"];

	 	return await gradeRef(ID)
	 		.update(grade)
	 		.then(() => grade)
	 		.catch(error => error);

	 };
/**
 *
 */

 /**
  * Supprimer une categorie
  * @param String ID
  */
 const deleteRequestApi = async (ID) => {
 	return await gradeRef(ID)
 		.remove()
 		.then(() => toast(`🦄 Bien! Grade bien supprimé`))
 		.catch(error => error);
 };

 /**
  * Annuler la suppression d'un utilisateur
  * @param {A} user
  */
 const undoDeleteRequestApi = async user => {
 	return await gradeRef(user)
 		.update({ isDeleted: true })
 		.then(() => user)
 		.catch(error => error);
 };

const getGrades = () => gradeRef();

export { addGradeRequestApi, editGradeRequestApi, getGrades, deleteRequestApi, undoDeleteRequestApi };
