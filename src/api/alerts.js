import { db } from "../firebase";
import { toast } from "react-toastify";

const alertsRef = (ID = null) => {
	let path = ID ? `alerts/${ID}` : "alerts";
	return db.ref(path);
};
/**
 * Création nouvelle expertise
 * @param {*} alert
 */
const addAlertsRequestApi = async alert => {
	alert.createdAt = new Date().getTime();
	return await alertsRef()
		.push(alert)
		.then(ref => ref.key)
		.catch(error => error);
};

/**
 *
 */
const getAlerts = () => alertsRef();

export { addAlertsRequestApi, getAlerts };
