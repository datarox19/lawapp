import { db } from "../firebase";
import { toast } from "react-toastify";

const roleRef = (ID = null) => {
	let path = ID ? `role/${ID}` : "role";
	return db.ref(path);
};
/**
 * Création nouvelle catégorie
 * @param Object role
 */
const addRoleRequestApi = async role => {
	role.createdAt = new Date().getTime();
	return await roleRef()
		.push(role)
		.then(ref => ref.key)
		.catch(error => error);
};
/**
 * Modification d'une categorie
 * @param {*} expertise
 */
 const editRoleRequestApi = async (role)  => {
 	const ID = role.roleID;
 	delete role["roleID"];

	 	return await roleRef(ID)
	 		.update(role)
	 		.then(() => role)
	 		.catch(error => error);

	 };
/**
 *
 */

 /**
  * Supprimer une categorie
  * @param String ID
  */
 const deleteRequestApi = async (ID) => {
 	return await roleRef(ID)
 		.remove()
 		.then(() => toast(`🦄 Bien! Role bien supprimé`))
 		.catch(error => error);
 };

 /**
  * Annuler la suppression d'un utilisateur
  * @param {A} user
  */
 const undoDeleteRequestApi = async user => {
 	return await roleRef(user)
 		.update({ isDeleted: true })
 		.then(() => user)
 		.catch(error => error);
 };

const getRoles = () => roleRef();

export { addRoleRequestApi, editRoleRequestApi, getRoles, deleteRequestApi, undoDeleteRequestApi };
