import { LOGOUT_USER } from "constants/actionTypes";

const INIT_STATE = {
  user: null,
  loading: false
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOGOUT_USER:
      return { ...state, user: null };
    default:
      return { ...state };
  }
};
