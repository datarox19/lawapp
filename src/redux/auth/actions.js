import { LOGOUT_USER } from "constants/actionTypes";

export const logoutUser = () => ({
  type: LOGOUT_USER
});

export * from "./asyncActions";
